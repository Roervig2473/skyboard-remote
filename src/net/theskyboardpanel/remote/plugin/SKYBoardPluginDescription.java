/**
 * 
 */
package net.theskyboardpanel.remote.plugin;

import net.theskyboardpanel.remoteapi.command.Command;
import net.theskyboardpanel.remoteapi.common.Author;
import net.theskyboardpanel.remoteapi.common.Version;
import net.theskyboardpanel.remoteapi.plugin.PluginDescription;

/**
 * 
 * This class is used to do the plugin descriptions
 * 
 * @author Roe
 *
 */
public class SKYBoardPluginDescription implements PluginDescription 
{

	private String name;
	private String main;
	private Author author;
	private Author[] authors;
	private Command[] commands;
	private Version version;
	private String[] dependencies;
	
	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginDescription#getName()
	 */
	@Override
	public String getName()
	{

		return this.name;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginDescription#getMain()
	 */
	@Override
	public String getMain()
	{
	
		return this.main;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginDescription#getAuthor()
	 */
	@Override
	public Author getAuthor()
	{
	
		return this.author;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginDescription#getAuthors()
	 */
	@Override
	public Author[] getAuthors()
	{
	
		return this.authors;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginDescription#getCommands()
	 */
	@Override
	public Command[] getCommands()
	{

		return this.commands;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginDescription#getVersion()
	 */
	@Override
	public Version getVersion()
	{

		return this.version;
	
	}


	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginDescription#getVersion()
	 */
	@Override
	public String[] getDependencies()
	{

		return this.dependencies;
	
	}

}
