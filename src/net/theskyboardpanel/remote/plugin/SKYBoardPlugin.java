/**
 * 
 */
package net.theskyboardpanel.remote.plugin;

import java.io.File;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remoteapi.logging.Logger;
import net.theskyboardpanel.remoteapi.plugin.Plugin;
import net.theskyboardpanel.remoteapi.plugin.PluginDescription;
import net.theskyboardpanel.remoteapi.plugin.PluginManager;

/**
 * @author Roe
 *
 */
public class SKYBoardPlugin extends Plugin
{
	
	private boolean enabled;
	private Logger logger;
	private PluginDescription pluginDescription;
	private File location;

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.Plugin#getPluginManager()
	 */
	@Override
	public PluginManager getPluginManager()
	{
		
		return Remote.getPluginManager();
	
	}

	@Override
	public File getLocation()
	{

		if(!this.location.exists())
		{
			
			this.location.mkdirs();
			
		}
		
		return this.location;
	
	}

	@Override
	public boolean isEnabled()
	{

		return this.enabled;
	
	}

	@Override
	public Logger getLogger()
	{

		return this.logger;
	
	}

	@Override
	public PluginDescription getPluginDescription()
	{

		return this.pluginDescription;
	
	}

}
