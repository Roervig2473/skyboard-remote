package net.theskyboardpanel.remote.plugin;

import java.io.*;
import java.lang.reflect.Method;
import java.net.*;
import java.util.ArrayList;
import java.util.jar.*;
import java.util.logging.Level;

import org.json.*;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remote.command.*;
import net.theskyboardpanel.remote.command.commands.*;
import net.theskyboardpanel.remote.common.*;
import net.theskyboardpanel.remote.event.EventManager;
import net.theskyboardpanel.remote.utils.logging.SKYBoardPluginLogger;
import net.theskyboardpanel.remoteapi.command.*;
import net.theskyboardpanel.remoteapi.common.*;
import net.theskyboardpanel.remoteapi.connection.Connection;
import net.theskyboardpanel.remoteapi.event.*;
import net.theskyboardpanel.remoteapi.permission.InsufficientPermissionsException;
import net.theskyboardpanel.remoteapi.plugin.*;
import net.theskyboardpanel.remoteapi.plugin.exceptions.*;
import net.theskyboardpanel.remoteapi.plugin.exceptions.IllegalAccessException;
import static net.theskyboardpanel.remote.utils.ReflectionUtils.*;

/**
 * @author Roe
 *
 */
public class SKYBoardPluginManager implements PluginManager
{

	private Plugin[] plugins;
	private CommandManager commandManager;
	private EventManager eventManager;
	
	public SKYBoardPluginManager()
	{
		
		this.setCommandManager(new CommandManager());
		this.setEventManager(new EventManager());
		this.setupCommands();
		
	}
	
	private void setupCommands()
	{
	
		CommandShutdown shutdown = new CommandShutdown();
		this.getCommandManager().registerCommand(shutdown, null);
		this.setCommandExecutor(shutdown, shutdown, null);
	
		CommandCheck check = new CommandCheck();
		this.getCommandManager().registerCommand(check, null);
		this.setCommandExecutor(check, check, null);
	
		CommandReload reload = new CommandReload();
		this.getCommandManager().registerCommand(reload, null);
		this.setCommandExecutor(reload, reload, null);
		
		
	}
	
	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginManager#getPlugin(java.lang.String)
	 */
	@Override
	public Plugin getPlugin(String pluginName)
	{
		
		for(Plugin plugin : this.getPlugins())
		{
			
			if(plugin.getPluginDescription().getName().equalsIgnoreCase(pluginName))
			{
				
				return plugin;
				
			}
			
		}
		
		return null;
	
	}
	
	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginManager#getPlugins()
	 */
	@Override
	public Plugin[] getPlugins()
	{
		
		return this.plugins;
	
	}
	
	private void setPlugins(Plugin[] plugins)
	{
		
		this.plugins = plugins;
		
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginManager#setCommandExecutor(net.theskyboardpanel.remoteapi.command.Command, net.theskyboardpanel.remoteapi.command.CommandExecutor, net.theskyboardpanel.remoteapi.plugin.Plugin)
	 */
	@Override
	public void setCommandExecutor(Command command, CommandExecutor executor, Plugin plugin)
	{

		this.getCommandManager().setCommandExectutor(command, executor);
		
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginManager#getCommand(java.lang.String)
	 */
	@Override
	public Command getCommand(String command)
	{
	
		try
		{
			
			return this.getCommandManager().getCommand(command, false);
			
		}
		catch(UnknownCommandException e)
		{
			
		}
	
		return null;
		
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginManager#loadPlugin(java.io.File)
	 */
	@Override
	public Plugin loadPlugin(File file) throws InvalidPluginException, InvalidPluginDescriptionException, InvalidDependenciesException
	{
		
		try
		{

			PluginDescription description = this.loadPluginDescription(file);
			this.addClasspath(file);
			SKYBoardPlugin plugin = (SKYBoardPlugin) Class.forName(description.getMain()).newInstance();
			setField(SKYBoardPlugin.class, "pluginDescription", description, plugin);
			Remote.getLogger().log("Loading plugin " + plugin.getPluginDescription().getName());
			setField(SKYBoardPlugin.class, "location", new File(new File(Remote.getArgumentParser().getArgument("pluginsdir", "plugins").getValue()), plugin.getPluginDescription().getName()), plugin);
			setField(SKYBoardPlugin.class, "logger", new SKYBoardPluginLogger(plugin), plugin);
			
			for(Command command : plugin.getPluginDescription().getCommands())
			{
				
				if(this.getCommandManager().registerCommand(command, plugin) != null)
				{
					
					continue;
					
				}
				
				this.setCommandExecutor(command, plugin, plugin);
				
			}
			
			if(!plugin.getLocation().exists())
			{
				
				plugin.getLocation().mkdirs();
				
			}
			
			plugin.onLoad();
			
			return plugin;
		
		}
		catch (Exception e)
		{

			InvalidPluginException ipe = new InvalidPluginException(e.getMessage());
			ipe.setStackTrace(e.getStackTrace());
			
			throw ipe;
		
		}
	
	}

	@Override
	public PluginDescription loadPluginDescription(File file) throws InvalidPluginDescriptionException 
	{

		if(file == null)
		{
			
			throw new InvalidPluginDescriptionException("Plugin File is null");
			
		}
		
		try
		{
		
			
			JarFile jar = new JarFile(file);
			JarEntry descriptionFile = jar.getJarEntry("plugin.json");
			
			if(descriptionFile == null)
			{
				
				throw new InvalidPluginDescriptionException("No plugin.json (" + file.toString() + ")");
				
			}
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(jar.getInputStream(descriptionFile)));
			
			String json = "", line = "";
			
			while((line = reader.readLine()) != null)
			{
				
				json += line;
				
			}
			
			JSONObject object = new JSONObject(json);
			PluginDescription description = new SKYBoardPluginDescription();

			setField(SKYBoardPluginDescription.class, "name", object.getString("name"), description);
			setField(SKYBoardPluginDescription.class, "main", object.getString("main"), description);
			
			ArrayList<String> dependencies = new ArrayList<String>();
			JSONArray dependencyArray = object.optJSONArray("dependencies");
			
			if(dependencyArray != null)
			{
				
				for(int i = 0; i < dependencyArray.length(); i++)
				{
					
					try
					{
						
						dependencies.add(dependencyArray.getString(i));
						
					}
					catch(JSONException e)
					{
						
					}
					
				}
				
			}
			
			setField(SKYBoardPluginDescription.class, "dependencies", dependencies.size() == 0 ? new String[0] : dependencies.toArray(new String[0]), description);
			JSONObject authorObject = object.getJSONObject("author");
			Author author = new SKYBoardAuthor();
	
			setField(SKYBoardAuthor.class, "name", authorObject.getString("name"), author);
			setField(SKYBoardAuthor.class, "about", authorObject.optString("about"), author);
			
			if(authorObject.opt("about") != null)
			{
				
				setField(SKYBoardAuthor.class, "website", new URL(authorObject.optString("website")), author);
				
			}
			
			setField(SKYBoardPluginDescription.class, "author", author, description);
			
			Version version = new SKYBoardVersion();
			this.setVersion(object.getString("version"), version);
			setField(SKYBoardPluginDescription.class, "version", version, description);
			
			JSONArray commandArray = object.optJSONArray("commands");
			
			if(commandArray != null)
			{
				
				ArrayList<Command> commands = new ArrayList<Command>();
				
				for(int i = 0; i < commandArray.length(); i++)
				{
					
					JSONObject commandObject = commandArray.optJSONObject(i);
					
					if(commandObject != null)
					{
					
						String name = commandObject.optString("name");
						String commandString = commandObject.optString("command");
						String commandDescription = commandObject.optString("description");
						String alternativesString = commandObject.optString("alternatives");
						String[] alternatives = null;
						
						if(commandString == null)
						{
							
							continue;
							
						}
						
						if(name == null)
						{
							
							name = commandString;
							
						}
						
						if(commandDescription == null)
						{
							
							commandDescription = "A description isn't available!";
							
						}
						
						if(alternativesString != null)
						{
							
							if(alternativesString.contains(" "))
							{
								
								alternatives = alternativesString.split(" ");
								
							}
							else
							{
								
								alternatives = new String[]
							    {
										
									alternativesString
								
							    };
								
							}
							
						}
						else
						{
							
							alternatives = new String[0];
							
						}
						
						commands.add(new SKYBoardCommand(name, commandString, commandDescription, alternatives));
						
					}
					
				}
				
				Command[] commandsArray = new Command[commands.size()];
				
				for(int i = 0; i < commands.size(); i++)
				{
					
					commandsArray[i] = commands.get(i);
					
				}
				
				setField(SKYBoardPluginDescription.class, "commands", commandsArray, description);
				
			}
			else
			{
				
				setField(SKYBoardPluginDescription.class, "commands", new Command[0], description);
				
			}
			
			return description;
			
		}
		catch (IOException e)
		{
			
			InvalidPluginDescriptionException ipe = new InvalidPluginDescriptionException("Invalid plugin.json");
			ipe.setStackTrace(e.getStackTrace());
			
			throw ipe;
		
		}
		catch(JSONException e)
		{
			
			InvalidPluginDescriptionException ipe = new InvalidPluginDescriptionException("Invalid plugin.json");
			ipe.setStackTrace(e.getStackTrace());
			
			throw ipe;
			
		}
	
	}
	
	private void setVersion(String version, Object instance)
	{
		
		if(!version.contains("."))
		{
			
			if(!version.contains("-"))
			{
				
				setField(SKYBoardVersion.class, "major", Integer.parseInt(version), instance);
				setField(SKYBoardVersion.class, "minor", 0, instance);
				setField(SKYBoardVersion.class, "patch", 0, instance);
				setField(SKYBoardVersion.class, "state", State.UNKNOWN, instance);
				
				return;
				
			}
			
			String[] versionArray = version.split("-");

			setField(SKYBoardVersion.class, "major", Integer.parseInt(versionArray[0]), instance);
			setField(SKYBoardVersion.class, "minor", 0, instance);
			setField(SKYBoardVersion.class, "patch", 0, instance);
			setField(SKYBoardVersion.class, "state", State.getState(versionArray[1]), instance);
			
			return;
			
		}
		
		String[] versionArray = version.split("\\.");
		
		if(versionArray.length == 2)
		{

			setField(Version.class, "major", versionArray[0], instance);
			
			if(versionArray[1].contains("-"))
			{
				
				String[] stateArray = versionArray[1].split("-");
				setField(SKYBoardVersion.class, "minor", Integer.parseInt(stateArray[0]), instance);
				setField(SKYBoardVersion.class, "state", State.getState(stateArray[1]), instance);
				
			}
			else
			{
				
				setField(SKYBoardVersion.class, "minor", Integer.parseInt(versionArray[1]), instance);
				
			}
			
			setField(Version.class, "patch", 0, instance);
			
		}
		else if(versionArray.length == 3)
		{

			setField(SKYBoardVersion.class, "major", Integer.parseInt(versionArray[0]), instance);
			setField(SKYBoardVersion.class, "minor", Integer.parseInt(versionArray[1]), instance);

			if(versionArray[2].contains("-"))
			{
				
				String[] stateArray = versionArray[2].split("-");
				setField(SKYBoardVersion.class, "patch", Integer.parseInt(stateArray[0]), instance);
				setField(SKYBoardVersion.class, "state", State.getState(stateArray[1]), instance);
				
			}
			else
			{

				setField(SKYBoardVersion.class, "patch", Integer.parseInt(versionArray[2]), instance);
				setField(SKYBoardVersion.class, "state", State.UNKNOWN, instance);
				
			}
			
		}
		else
		{

			setField(SKYBoardVersion.class, "major", Integer.parseInt(version), instance);
			
		}
		
	}
	
	private void addClasspath(File file) throws Exception
	{
		
		Method method = URLClassLoader.class.getDeclaredMethod("addURL", new Class[]
		{
				
			URL.class
				
		});
        
		method.setAccessible(true);
        method.invoke(ClassLoader.getSystemClassLoader(), new Object[]
        {
        		
        	file.toURI().toURL()
        		
        });
        
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginManager#loadPlugins(java.io.File)
	 */
	@Override
	public Plugin[] loadPlugins(File directory)
	{
		
		if(directory == null)
		{
			
			throw new IllegalArgumentException("The directory cannot be null");
			
		}
		
		Remote.getLogger().log("Loading plugins from directory: " + directory.getAbsolutePath());
		
		if(!directory.exists())
		{
			
			directory.mkdirs();
			
		}
		
		if(!directory.isDirectory())
		{
			
			throw new IllegalArgumentException("The plugins must load from a directory");
			
		}
		
		ArrayList<Plugin> plugins = new ArrayList<Plugin>();
		
		for(File file : directory.listFiles(new FileFilter()
		{

			@Override
			public boolean accept(File file)
			{

				return file.getName().endsWith(".jar");
			
			}
			
		}))
		{
			
			try
			{
				
				plugins.add(this.loadPlugin(file));
				
			}
			catch(Exception e)
			{

				Remote.getLogger().log(e);
				
			}
			
		}

		this.setPlugins(plugins.toArray(new Plugin[plugins.size()]));
		
		return this.getPlugins();
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginManager#clearPlugins()
	 */
	@Override
	public void clearPlugins()
	{

		this.disablePlugins();
		this.setPlugins(new Plugin[0]);

	}

	/**
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginManager#disablePlugin(net.theskyboardpanel.remoteapi.plugin.Plugin)
	 */
	@Override
	public void disablePlugin(Plugin plugin)
	{
		
		if(!plugin.isEnabled())
		{
			
			return;
			
		}
		
		plugin.getLogger().log("Disabling: v" + plugin.getPluginDescription().getVersion().toString());
		plugin.onDisable();

	}

	/**
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginManager#disablePlugins()
	 */
	@Override
	public void disablePlugins()
	{

		for(Plugin plugin : this.getPlugins())
		{
			
			try
			{
				
				this.disablePlugin(plugin);
				
			}
			catch(Exception e)
			{
				
				Remote.getLogger().log("An internal error occoured while disabling " + plugin.getPluginDescription().getName() + " v" + plugin.getPluginDescription().getVersion().toString());
				Remote.getLogger().log(e);
				
			}
			
		}
		
	}

	/**
	 * @see net.theskyboardpanel.remoteapi.plugin.PluginManager#enablePlugins
	 */
	@Override
	public Plugin[] enablePlugins()
	{
		
		ArrayList<Plugin> plugins = new ArrayList<Plugin>();
		
		for(Plugin plugin : this.getPlugins())
		{
			
			try
			{
			
				if(plugin.isEnabled())
				{
					
					continue;
					
				}
				
				if(this.enablePlugin(plugin))
				{
					
					plugins.add(plugin);
					
				}
				
			}
			catch(Exception e)
			{
				
				Remote.getLogger().log("An internal error occoured while enabling " + plugin.getPluginDescription().getName() + " v" + plugin.getPluginDescription().getVersion().toString());
				Remote.getLogger().log(e);
				
			}
			
		}

		return plugins.toArray(new Plugin[plugins.size()]);
	
	}

	@Override
	public boolean enablePlugin(Plugin plugin)
	{

		plugin.getLogger().log("Enabling v" + plugin.getPluginDescription().getVersion().toString());
		setField(SKYBoardPlugin.class, "enabled", false, plugin);
		
		try
		{
			
			for(String depend : plugin.getPluginDescription().getDependencies())
			{
				
				Plugin dependency = this.getPlugin(depend);
				
				if(dependency == null)
				{
					
					plugin.getLogger().log(Level.WARNING, "I have unmet dependencies! (" + depend + ")");
					
					return false;
					
				}
				
				if(dependency.isEnabled())
				{
					
					continue;
					
				}
				
				setField(SKYBoardPlugin.class, "enabled", true, dependency);
				setField(SKYBoardPlugin.class, "enabled", this.enablePlugin(dependency), dependency);
				
				if(!dependency.isEnabled())
				{
					
					plugin.getLogger().log(Level.WARNING, "I have unmet dependencies! (" + depend + ")");
					
					return false;
					
				}
				
				
			}
			
			setField(SKYBoardPlugin.class, "enabled", true, plugin);
			setField(SKYBoardPlugin.class, "enabled", plugin.onEnable(), plugin);
			
			return plugin.isEnabled();
			
		}
		catch(Exception e)
		{
			
			Remote.getLogger().log(e);
			
			return false;
			
		}
		
	}

	@Override
	public void registerListener(Listener listener, Plugin plugin)
	{
		
		if(!plugin.isEnabled())
		{
			
			throw new IllegalAccessException(plugin, "register an event listener");
			
		}
		
		this.getEventManager().registerListener(listener, plugin);
		
	}

	@Override
	public void callEvent(Event event)
	{
		
		this.getEventManager().call(event);
		
	}
	
	public boolean registerConnectionType(Class<? extends Connection> connection, Plugin plugin)
	{
		
		return Remote.getServerManager().registerConnectionType(connection);
		
	}
	
	public boolean executeCommand(CommandSender sender, String commandString, String[] args) throws InsufficientPermissionsException
	{
	
		return this.getCommandManager().onCommand(sender, commandString, args);
		
	}

	private CommandManager getCommandManager()
	{
	
		return this.commandManager;
	
	}

	private void setCommandManager(CommandManager commandManager)
	{
	
		this.commandManager = commandManager;
	
	}

	private EventManager getEventManager()
	{
	
		return this.eventManager;
	
	}

	private void setEventManager(EventManager eventManager)
	{
	
		this.eventManager = eventManager;
	
	}

}