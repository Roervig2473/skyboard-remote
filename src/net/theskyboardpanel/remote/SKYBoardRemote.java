package net.theskyboardpanel.remote;

import java.io.File;

import com.javaportals.argumentsparser.Parser;

import net.theskyboardpanel.remote.common.SKYBoardVersion;
import net.theskyboardpanel.remote.plugin.SKYBoardPluginManager;
import net.theskyboardpanel.remote.server.ServerManager;
import net.theskyboardpanel.remote.user.Console;
import net.theskyboardpanel.remote.utils.ReflectionUtils;
import net.theskyboardpanel.remote.utils.logging.SKYBoardLogger;
import net.theskyboardpanel.remoteapi.Remote;
import net.theskyboardpanel.remoteapi.common.State;
import net.theskyboardpanel.remoteapi.plugin.PluginManager;

/**
 * 
 * This is the primary remote class
 * 
 * @author Roe
 *
 */
public class SKYBoardRemote extends Remote
{
	
	private static SKYBoardRemote remote;
	private boolean running;
	private SKYBoardVersion version;
	private Parser argumentParser;
	private SKYBoardLogger logger;
	private PluginManager pluginManager;
	private Console console;
	private ServerManager serverManager;
	
	/**
	 * 
	 * @param args the arguments to pass to the remote
	 */
	public SKYBoardRemote(String[] args)
	{
		
		long start = System.currentTimeMillis();
		this.setRemote(this);
		Thread.currentThread().setName("SKYBoard Remote Primary Thread");
		this.setRunning(true);
		this.setVersion();
		Runtime.getRuntime().addShutdownHook(new ShutdownHook());
		this.setArgumentParser(new Parser(args));
		this.getArgumentParser().parse();
		this.setLogger(new SKYBoardLogger());
		this.getLogger().log("Starting SKYBoard Remote (" + this.getVersion().toString() + ")");
		this.setPluginManager(new SKYBoardPluginManager());
		this.setConsole(new Console());
		this.getConsole().start();
		this.getPluginManager().loadPlugins(new File(this.getArgumentParser().getArgument("pluginsDir", "plugins").getValue()));
		this.getPluginManager().enablePlugins();
		this.setServerManager(new ServerManager());
		this.getLogger().log("Done! (Time: " + (System.currentTimeMillis() - start) + "ms, Plugins: " + this.getPluginManager().getPlugins().length + ")");
		
	}
	
	private void setVersion()
	{

		this.setVersion(new SKYBoardVersion());
		ReflectionUtils.setField(SKYBoardVersion.class, "major", 1, this.getVersion());
		ReflectionUtils.setField(SKYBoardVersion.class, "minor", 3, this.getVersion());
		ReflectionUtils.setField(SKYBoardVersion.class, "patch", 0, this.getVersion());
		ReflectionUtils.setField(SKYBoardVersion.class, "state", State.DEVELOPMENT, this.getVersion());
		
	}

	/**
	 * 
	 * @param args the arguments to pass to the remote
	 */
	public static void main(String[] args)
	{
		
		new SKYBoardRemote(args);
		
	}
	
	/**
	 * 
	 * This method is used to get the remote instance
	 * 
	 * @return the remote
	 */
	public static SKYBoardRemote getRemote()
	{
		
		return remote;
		
	}
	
	private void setRemote(SKYBoardRemote remoteInstance)
	{
		
		remote = remoteInstance;
		
	}
	
	public boolean isRunning()
	{
	
		return this.running;
	
	}

	public void setRunning(boolean running)
	{
	
		this.running = running;
	
	}

	public SKYBoardVersion getVersion()
	{
	
		return this.version;
	
	}

	private void setVersion(SKYBoardVersion version)
	{
	
		this.version = version;
	
	}

	/**
	 * 
	 * This method is used to get the argument parser
	 * 
	 * @return the argument parser
	 */
	public Parser getArgumentParser()
	{
		
		return this.argumentParser;
		
	}
	
	private void setArgumentParser(Parser parser)
	{
		
		this.argumentParser = parser;
		
	}
	
	/**
	 * 
	 * This method is used to get the root logger of the sytem. It's not recommended to use this logger for the plugins since they get their own.
	 * 
	 * @return the primary logger
	 */
	public SKYBoardLogger getLogger()
	{
	
		return this.logger;
	
	}

	private void setLogger(SKYBoardLogger logger)
	{
	
		this.logger = logger;
	
	}

	/**
	 * 
	 * This method is used to get the plugin manager
	 * 
	 * @return the plugin manager;
	 */
	public PluginManager getPluginManager()
	{
		
		return this.pluginManager;
		
	}
	
	private void setPluginManager(PluginManager pluginManager)
	{
		
		this.pluginManager = pluginManager;
		
	}

	public Console getConsole()
	{
	
		return this.console;
	
	}

	private void setConsole(Console console)
	{
	
		this.console = console;
	
	}

	public ServerManager getServerManager()
	{
	
		return this.serverManager;
	
	}

	public void setServerManager(ServerManager serverManager)
	{
	
		this.serverManager = serverManager;
	
	}
	
}