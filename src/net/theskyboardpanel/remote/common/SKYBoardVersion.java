/**
 * 
 */
package net.theskyboardpanel.remote.common;

import net.theskyboardpanel.remoteapi.common.State;
import net.theskyboardpanel.remoteapi.common.Version;

/**
 * @author Roe
 *
 */
public class SKYBoardVersion implements Version
{

	private int major;
	private int minor;
	private int patch;
	private State state;
	
	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.common.Version#getMajor()
	 */
	@Override
	public int getMajor()
	{

		return this.major;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.common.Version#getMinor()
	 */
	@Override
	public int getMinor()
	{

		return this.minor;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.common.Version#getPatch()
	 */
	@Override
	public int getPatch()
	{

		return this.patch;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.common.Version#getState()
	 */
	@Override
	public State getState()
	{

		return this.state;
	
	}
	
	public String toString()
	{
		
		return this.getMajor() + "." + this.getMinor() + "." + this.getPatch() + "-" + this.getState(); 
		
	}

}
