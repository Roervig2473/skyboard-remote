package net.theskyboardpanel.remote.common;

import java.net.URL;

import net.theskyboardpanel.remoteapi.common.Author;

public class SKYBoardAuthor implements Author
{

	private String name;
	private String about;
	private URL website;

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.common.Author#getName()
	 */
	@Override
	public String getName()
	{

		return this.name;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.common.Author#getAbout()
	 */
	@Override
	public String getAbout() 
	{
	
		return this.about;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.common.Author#getWebsite()
	 */
	@Override
	public URL getWebsite()
	{
	
		return this.website;
	
	}
	
}