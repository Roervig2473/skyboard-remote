package net.theskyboardpanel.remote;

import net.theskyboardpanel.remote.server.ServerManager;
import net.theskyboardpanel.remote.user.Console;
import net.theskyboardpanel.remote.utils.logging.SKYBoardLogger;
import net.theskyboardpanel.remoteapi.event.Event;
import net.theskyboardpanel.remoteapi.plugin.PluginManager;

import com.javaportals.argumentsparser.Parser;

public class Remote
{

	public static Parser getArgumentParser()
	{
		
		return SKYBoardRemote.getRemote().getArgumentParser();
		
	}
	
	public static PluginManager getPluginManager()
	{
		
		return SKYBoardRemote.getRemote().getPluginManager();
		
	}
	
	public static SKYBoardLogger getLogger()
	{
		
		return SKYBoardRemote.getRemote().getLogger();
		
	}
	
	public static void shutdown()
	{
		
		System.exit(2);
		
	}
	
	public static Console getConsole()
	{
		
		return SKYBoardRemote.getRemote().getConsole();
		
	}
	
	public static ServerManager getServerManager()
	{
		
		return SKYBoardRemote.getRemote().getServerManager();
		
	}
	
	public static void callEvent(Event event)
	{

		getPluginManager().callEvent(event);
		
	}
	
}