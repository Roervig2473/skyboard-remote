/**
 * 
 */
package net.theskyboardpanel.remote.user;

import java.io.*;
import java.util.ArrayList;

import net.theskyboardpanel.remote.*;
import net.theskyboardpanel.remoteapi.command.CommandSender;
import net.theskyboardpanel.remoteapi.permission.*;

/**
 * @author Roe
 *
 */
public class Console extends Thread implements CommandSender
{
	
	public Console()
	{
		
		super("Console");
		
	}
	
	public void run()
	{
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String in = "";
		Remote.getLogger().log("Starting command listener");
		
		while(SKYBoardRemote.getRemote().isRunning())
		{
		
			try
			{
			
				while((in = reader.readLine()) != null)
				{
					
					if(in.contains(" "))
					{
						
						String[] fullArgs = in.split(" ");
						String[] args = new String[fullArgs.length - 1];
						
						for(int i = 1; i < (fullArgs.length); i++)
						{
						
							args[i - 1] = fullArgs[i];
							
						}
						
						try
						{
							
							this.executeCommand(fullArgs[0], args);
							
						}
						catch(InsufficientPermissionsException e)
						{
							
							this.sendMessage(e.getMessage());
							
						}
						
					}
					else
					{
						
						try
						{
							
							this.executeCommand(in, new String[0]);
							
						}
						catch(InsufficientPermissionsException e)
						{
							
							this.sendMessage(e.getMessage());
							
						}
						
					}
					
				}
			
			}
			catch (IOException e)
			{
				
				Remote.getLogger().log(e);
			
			}
			
		}
		
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#hasPermission(java.lang.String)
	 */
	@Override
	public boolean hasPermission(String permission)
	{
	
		return true;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#hasPermission(net.theskyboardpanel.remoteapi.permission.Permission)
	 */
	@Override
	public boolean hasPermission(Permission permission)
	{
	
		return true;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#getPermissions()
	 */
	@Override
	public ArrayList<Permission> getPermissions()
	{

		return new ArrayList<Permission>();
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#addPermission(net.theskyboardpanel.remoteapi.permission.Permission)
	 */
	@Override
	public void addPermission(Permission permission)
	{

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#addPermission(java.lang.String)
	 */
	@Override
	public void addPermission(String permission)
	{

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#removePermission(net.theskyboardpanel.remoteapi.permission.Permission)
	 */
	@Override
	public void removePermission(Permission permission)
	{

	}
	
	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#removePermission(java.lang.String)
	 */
	@Override
	public void removePermission(String permission)
	{

	}
	
	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.command.CommandSender#sendMessage(java.lang.String)
	 */
	@Override
	public void sendMessage(String message)
	{

		Remote.getLogger().log(message);

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.command.CommandSender#executeCommand(java.lang.String, java.lang.String[])
	 */
	@Override
	public boolean executeCommand(String command, String[] args)
	{
		
		return Remote.getPluginManager().executeCommand(this, command, args);
	
	}

	@Override
	public String getUsername()
	{
		
		return "Console";
	
	}

	@Override
	public void setUsername(String username)
	{
		
		throw new RuntimeException("You can't change the username of the console!");
		
	}

}
