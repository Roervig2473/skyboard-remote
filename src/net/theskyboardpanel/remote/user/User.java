package net.theskyboardpanel.remote.user;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remoteapi.command.CommandSender;
import net.theskyboardpanel.remoteapi.connection.Connection;
import net.theskyboardpanel.remoteapi.connection.exceptions.ConnectionException;
import net.theskyboardpanel.remoteapi.packet.Packet;
import net.theskyboardpanel.remoteapi.permission.*;

public class User extends Connection implements CommandSender
{
	
	public User(Socket socket, ObjectOutputStream out, ObjectInputStream in)
	{
	
		super(socket, out, in);

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#hasPermission(java.lang.String)
	 */
	@Override
	public boolean hasPermission(String permission)
	{
	
		return true;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#hasPermission(net.theskyboardpanel.remoteapi.permission.Permission)
	 */
	@Override
	public boolean hasPermission(Permission permission)
	{
	
		return true;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#getPermissions()
	 */
	@Override
	public ArrayList<Permission> getPermissions()
	{

		return new ArrayList<Permission>();
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#addPermission(net.theskyboardpanel.remoteapi.permission.Permission)
	 */
	@Override
	public void addPermission(Permission permission)
	{

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#addPermission(java.lang.String)
	 */
	@Override
	public void addPermission(String permission)
	{

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#removePermission(net.theskyboardpanel.remoteapi.permission.Permission)
	 */
	@Override
	public void removePermission(Permission permission)
	{

	}
	
	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.permission.Permissionable#removePermission(java.lang.String)
	 */
	@Override
	public void removePermission(String permission)
	{

	}
	
	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.command.CommandSender#sendMessage(java.lang.String)
	 */
	@Override
	public void sendMessage(String message)
	{

		Remote.getLogger().log(message);

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.command.CommandSender#executeCommand(java.lang.String, java.lang.String[])
	 */
	@Override
	public boolean executeCommand(String command, String[] args)
	{
		
		return Remote.getPluginManager().executeCommand(this, command, args);
	
	}

	@Override
	public Packet getPacket() throws ConnectionException
	{

		return null;
	
	}

	@Override
	public Packet recievePacket() throws ConnectionException
	{

		return null;

	}

	@Override
	public void sendPacket(Packet packet) throws ConnectionException
	{
		
	}

	@Override
	public void close() throws ConnectionException
	{
		
	}

	@Override
	public String getUsername()
	{

		return null;
	
	}

	@Override
	public void setUsername(String username)
	{
		// TODO Auto-generated method stub
		
	}

}
