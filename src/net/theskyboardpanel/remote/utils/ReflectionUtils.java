package net.theskyboardpanel.remote.utils;

import java.lang.reflect.Field;

public class ReflectionUtils
{

	public static boolean setField(Class<?> clazz, String name, Object value, Object instance)
	{
		
		if(value == null)
		{
			
			return false;
			
		}
		
		try
		{

			Field field = clazz.getDeclaredField(name);
			field.setAccessible(true);
			field.set(instance, value);

			return true;
			
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
			
		}
		
		return false;
		
	}
	
}