package net.theskyboardpanel.remote.utils;

import java.net.*;
import java.io.*;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remote.server.Server;
import net.theskyboardpanel.remoteapi.packet.packets.status.*;

public class ServerUtils
{

	/**
	 * 
	 * This method is used to ping a internal server
	 * 
	 * @param server the server you want to ping
	 * @return the response time of the server
	 * @throws IOException thrown when an error occours
	 * @throws UnknownHostException thrown when the server host is invalid
	 * @throws ClassNotFoundException thrown when a bad response is given.
	 */
	public static long pingServer(Server server) throws UnknownHostException, IOException, ClassNotFoundException
	{

		long start = System.currentTimeMillis();
		Remote.getLogger().log("Pinging " + server.getName());
		Socket socket = new Socket(server.getIp(), server.getPort());
		ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
		ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
		out.writeUTF("net.theskyboardpanel.remote.connection.ConnectionJava");
		out.flush();
		out.writeObject(new PacketPing());
		out.flush();
		@SuppressWarnings("unused")
		PacketPong packet = (PacketPong) in.readObject();
		out.close();
		in.close();
		socket.close();
		
		return System.currentTimeMillis() - start;
		
	}

	/**
	 * 
	 * This method is used to ping a external SKYBoard Remote server
	 * 
	 * @param ip the ip of the server you want to ping
	 * @param port the port of the server you want to ping
	 * @return the response time of the server
	 * @throws IOException thrown when an error occours
	 * @throws UnknownHostException thrown when the server host is invalid
	 * @throws ClassNotFoundException thrown when a bad response is given.
	 */
	public static long pingServer(String ip, int port) throws UnknownHostException, IOException, ClassNotFoundException
	{

		long start = System.currentTimeMillis();
		Remote.getLogger().log("Pinging " + ip + ":" + port);
		Socket socket = new Socket(ip, port);
		ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
		ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
		out.writeUTF("net.theskyboardpanel.remote.connection.ConnectionJava");
		out.flush();
		out.writeObject(new PacketPing());
		out.flush();
		@SuppressWarnings("unused")
		PacketPong packet = (PacketPong) in.readObject();
		out.close();
		in.close();
		socket.close();
		
		return System.currentTimeMillis() - start;
		
	}
	
}