package net.theskyboardpanel.remote.utils;

import java.util.ArrayList;

public class ArrayUtils 
{

	public static String arrayToString(String[] array, String splitter)
	{
		
		String result = "";
		
		for(int roe = 0; roe < array.length; roe++)
		{
			
			result += array[roe] + splitter;
			
		}
		
		return result.substring(0, result.length() - splitter.length());
		
	}

	public static String arrayToString(ArrayList<String> array, String splitter)
	{
		
		return ArrayUtils.arrayToString((String[]) array.toArray(), splitter);
		
	}
	
	public static Object[] arrayListToArray(ArrayList<?> array)
	{
		
		Object[] objects = new Object[array.size()];
		
		for(int i = 0; i < array.size(); i++)
		{
			
			objects[i] = array.get(i);
			
		}
		
		return objects;
		
	}
	
}