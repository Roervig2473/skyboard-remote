package net.theskyboardpanel.remote.utils.logging;

import java.util.logging.*;

public class SKYBoardConsoleHandler extends Handler
{

	@Override
	public void close() throws SecurityException
	{
		
	}

	@Override
	public void flush()
	{
		
	}

	@Override
	public void publish(LogRecord record)
	{
	
		System.out.print(this.getFormatter().format(record));
		
	}
	
}