package net.theskyboardpanel.remote.utils.logging;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

import net.theskyboardpanel.remote.Remote;

/**
 * @author Roe
 *
 */
public class SKYBoardLogger implements net.theskyboardpanel.remoteapi.logging.Logger
{
	
	private Logger logger;
	private boolean debug = true;
	private File directory;
	private File log;
	
	public SKYBoardLogger()
	{
		
		this.setLogger(Logger.getLogger("SKYBoard Remote Logger"));
		this.getLogger().setUseParentHandlers(false);
		this.setDirectory(new File(Remote.getArgumentParser().getArgument("logsDir", "logs").getValue()));
		
		if(!this.getDirectory().exists())
		{
			
			this.getDirectory().mkdirs();
			
		}
		
		if(!this.getDirectory().isDirectory())
		{
			
			throw new IllegalArgumentException("logsDir has to be a directory!");
			
		}
		
		if(!this.getDirectory().canRead())
		{

			throw new IllegalArgumentException("The logs directory is not readable!");
			
		}
		
		if(!this.getDirectory().canWrite())
		{

			throw new IllegalArgumentException("The logs directory is not writeable!");
			
		}
		
		this.setLog(new File(this.getDirectory(), Remote.getArgumentParser().getArgument("logName", new SimpleDateFormat("HH-mm-ss-dd-MM-yyyy").format(new Date(System.currentTimeMillis())).toString() + ".log").getValue()));
		
		if(!this.getLog().exists())
		{
			
			try
			{
			
				this.getLog().getParentFile().mkdirs();
				this.getLog().createNewFile();
			
			}
			catch (IOException e)
			{

				e.printStackTrace();
				throw new IllegalArgumentException("Unable to create log file!");
			
			}
			
		}
		
		if(!this.getLog().canRead())
		{
			
			throw new IllegalArgumentException("The log file is not readable!");
			
		}
		
		if(!this.getLog().canWrite())
		{
			
			throw new IllegalArgumentException("The log file is not writeable!");
			
		}
		
		try
		{
		
			Handler handler = new FileHandler(this.getLog().toString(), true);
			handler.setFormatter(new SKYBoardFormatter());
			this.getLogger().addHandler(handler);
			handler = new SKYBoardConsoleHandler();
			handler.setFormatter(new SKYBoardFormatter());
			this.getLogger().addHandler(handler);
			
			File latestLog = new File(this.getDirectory(), "latest.log");
			
			if(!latestLog.exists())
			{
				
				try
				{
				
					latestLog.getParentFile().mkdirs();
					latestLog.createNewFile();
				
				}
				catch (IOException e)
				{

					this.log(e);
					
					return;
				
				}
				
			}
			else
			{
				
				latestLog.delete();
				latestLog.createNewFile();
				
			}
			
			if(!latestLog.canRead())
			{
				
				this.log(Level.WARNING, "The latest log file (" + latestLog.getPath() + ") is not readable!");
				
				return;
				
			}
			
			if(!this.getLog().canWrite())
			{
				
				this.log(Level.WARNING, "The latest log file (" + latestLog.getPath() + ") is not writeable!");
				
				return;
				
			}
			
			handler = new FileHandler(latestLog.toString(), true);
			handler.setFormatter(new SKYBoardFormatter());
			this.getLogger().addHandler(handler);
		
		}
		catch (SecurityException e)
		{

			e.printStackTrace();
			System.exit(22345);
		
		}
		catch (IOException e)
		{

			e.printStackTrace();
			System.exit(22344);
		
		}
		
	}
	
	public void close()
	{
		
		for(Handler handler : this.getLogger().getHandlers())
		{
			
			handler.flush();
			handler.close();
			
		}
		
	}
	

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.logging.Logger#log(java.lang.String)
	 */
	@Override
	public void log(String message)
	{
		
		this.log(Level.INFO, message);

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.logging.Logger#log(java.util.logging.Level, java.lang.String)
	 */
	@Override
	public void log(Level level, String message)
	{

		this.getLogger().log(level, message);
		
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.logging.Logger#log(java.lang.Exception)
	 */
	@Override
	public void log(Exception e) 
	{
		
		this.log(Level.WARNING, e);

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.logging.Logger#log(java.util.logging.Level, java.lang.Exception)
	 */
	@Override
	public void log(Level level, Exception e) 
	{
		
		this.log(level, e.getMessage() == null || e.getMessage().equalsIgnoreCase("") ? e.getClass().getCanonicalName() : e.getMessage());
		
		if(this.isDebug())
		{
			
			for(int i = 0; i < e.getStackTrace().length; i++)
			{
				
				this.log(level, e.getStackTrace()[i].toString());
				
			}
			
		}

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.logging.Logger#isDebug()
	 */
	@Override
	public boolean isDebug()
	{
	
		return this.debug;
	
	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.logging.Logger#setDebug(boolean)
	 */
	@Override
	public void setDebug(boolean debug)
	{
		
		this.debug = debug;

	}


	public Logger getLogger()
	{
	
		return this.logger;
	
	}


	private void setLogger(Logger logger) 
	{
	
		this.logger = logger;
	
	}


	private File getDirectory()
	{
	
		return directory;
	
	}


	private void setDirectory(File directory)
	{
	
		this.directory = directory;
	
	}


	private File getLog()
	{
	
		return log;
	
	}


	private void setLog(File log)
	{
	
		this.log = log;
	
	}

}
