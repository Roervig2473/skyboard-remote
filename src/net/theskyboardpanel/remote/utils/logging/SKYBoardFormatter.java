package net.theskyboardpanel.remote.utils.logging;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

public class SKYBoardFormatter extends Formatter
{

	@Override
	public String format(LogRecord record)
	{
		
		StringBuffer sb = new StringBuffer();
		Date date = new Date(record.getMillis());
		sb.append("[").append(new SimpleDateFormat("HH:mm:ss dd/MM-yyyy").format(date).toString()).append("] [")
		.append(record.getLevel())
		.append("] ").append(record.getMessage()).append("\n");
		
		return sb.toString();
	
	}
	
}