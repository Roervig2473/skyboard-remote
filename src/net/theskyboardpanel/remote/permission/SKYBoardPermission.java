package net.theskyboardpanel.remote.permission;

import net.theskyboardpanel.remoteapi.permission.Permission;

public class SKYBoardPermission implements Permission 
{

	private String name;
	private String permission;
	private String description;
	
	public SKYBoardPermission(String name, String permission, String description)
	{
		
		this.setName(name);
		this.setPermission(permission);
		this.setDescription(description);
		
	}
	
	@Override
	public String getName()
	{

		return this.name;
		
		
	}

	@Override
	public void setName(String name)
	{

		this.name = name;

	}

	@Override
	public String getPermission()
	{

		return this.permission;
	
	}

	@Override
	public void setPermission(String permission)
	{

		this.permission = permission;

	}

	@Override
	public String getDescription()
	{

		return this.description;
	
	}

	@Override
	public void setDescription(String description)
	{

		this.description = description;

	}

}
