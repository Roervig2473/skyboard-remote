package net.theskyboardpanel.remote;

import net.theskyboardpanel.remote.utils.logging.SKYBoardLogger;

public class ShutdownHook extends Thread
{

	public void run()
	{
		
		Remote.getLogger().log("Shutting down");
		SKYBoardRemote.getRemote().setRunning(false);
		Remote.getServerManager().close();
		Remote.getPluginManager().disablePlugins();
		((SKYBoardLogger) Remote.getLogger()).close();
		
	}
	
}