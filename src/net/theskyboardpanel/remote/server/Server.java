package net.theskyboardpanel.remote.server;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.*;
import java.util.ArrayList;
import java.util.logging.Level;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remoteapi.connection.Connection;
import net.theskyboardpanel.remoteapi.connection.exceptions.ConnectionException;

public class Server extends Thread
{

	private ServerSocket serverSocket;
	private boolean shouldAccept;
	private String ip;
	private int port;
	private ArrayList<Connection> connections;
	
	public Server(String ip, int port) throws IOException
	{
		
		super("Server hosted on " + ip + ":" + port);
		this.setServerSocket(new ServerSocket());
		this.setIp(ip);
		this.setPort(port);
		this.setShouldAccept(true);
		this.setConnections(new ArrayList<Connection>());
		this.bind(this.getIp(), this.getPort());
		
	}
	
	public void bind(String ip, int port) throws IOException
	{
	
		Remote.getLogger().log("Binding a " + this.getName());
		
		this.getServerSocket().bind(new InetSocketAddress(ip, port));
		
	}
	
	public void run()
	{
		
		Remote.getLogger().log("Running " + this.getName());
		
		while(!this.getServerSocket().isClosed())
		{
			
			if(!this.shouldAccept())
			{
				
				if(this.getConnections().size() == 0)
				{
					
					try
					{

						Remote.getLogger().log(this.getName() + " have no connections left! Closing...");
						this.close();
					
					}
					catch (IOException e)
					{

						Remote.getLogger().log(e);
						
						return;
					
					}
					
				}
				
			}
			
			Socket socket = null;
			
			try
			{
			
				socket = this.getServerSocket().accept();
				ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
				String connectionID = in.readUTF();
				
				if(connectionID != null)
				{
					
					try
					{

						Class<? extends Connection> connectionClass = Remote.getServerManager().getConnection(connectionID);
						
						if(connectionClass == null)
						{
							
							in.close();
							out.close();
							socket.close();
							
							return;
							
						}
						
						Connection connection = connectionClass.getConstructor(Socket.class, ObjectOutputStream.class, ObjectInputStream.class).newInstance(socket, out, in);
						connection.start();
						
						this.getConnections().add(connection);
					
					}
					catch (InstantiationException e)
					{
						
						Remote.getLogger().log(Level.SEVERE, e);
					
					}
					catch (IllegalAccessException e)
					{

						Remote.getLogger().log(Level.SEVERE, e);
					
					}
					catch (IllegalArgumentException e)
					{

						Remote.getLogger().log(Level.SEVERE, e);
					
					}
					catch (SecurityException e)
					{

						Remote.getLogger().log(Level.SEVERE, e);
					
					}
					catch (InvocationTargetException e)
					{

						Remote.getLogger().log(Level.SEVERE, e);
					
					}
					catch (NoSuchMethodException e)
					{

						Remote.getLogger().log(Level.SEVERE, e);
					
					}
					
				}
			
			}
			catch (IOException e)
			{

				if(this.getServerSocket().isClosed())
				{
					
					return;
					
				}
				
				Remote.getLogger().log(e);
				
				try
				{
				
					socket.close();
				
				}
				catch (IOException e1)
				{
					
					Remote.getLogger().log(e);

				}
				catch(NullPointerException e1)
				{
					
				}
			
			}
			
		}
		
	}
	
	public void close() throws IOException
	{
		
		for(Connection connection : this.getConnections())
		{
			
			try
			{
			
				connection.close();
			
			}
			catch (ConnectionException e)
			{

				Remote.getLogger().log(e);
			
			}
			
		}
		
		this.getServerSocket().close();
		
	}
	
	public ServerSocket getServerSocket() 
	{
	
		return serverSocket;

	}

	private void setServerSocket(ServerSocket serverSocket)
	{
	
		this.serverSocket = serverSocket;
	
	}
	
	public boolean shouldAccept()
	{
	
		return this.shouldAccept;
	
	}
	
	public void setShouldAccept(boolean shouldAccept)
	{
	
		this.shouldAccept = shouldAccept;
		
		if(!this.shouldAccept())
		{
			
			Remote.getLogger().log(this.getName() + " is no longer accepting connections!");
			
		}
	
	}

	public ArrayList<Connection> getConnections()
	{
	
		return this.connections;
	
	}

	private void setConnections(ArrayList<Connection> connections)
	{
	
		this.connections = connections;
	
	}

	public String getIp()
	{
	
		return ip;
	
	}

	private void setIp(String ip)
	{
	
		this.ip = ip;
	
	}

	public int getPort()
	{
	
		return port;
	
	}

	private void setPort(int port)
	{
	
		this.port = port;
	
	}
	
}