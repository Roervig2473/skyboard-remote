package net.theskyboardpanel.remote.server;

import java.io.*;
import java.net.*;
import java.util.logging.Level;

import net.theskyboardpanel.remote.*;
import net.theskyboardpanel.remote.event.server.monitor.ServerCheckEvent;
import net.theskyboardpanel.remote.event.server.monitor.ServerDeadEvent;
import net.theskyboardpanel.remote.event.server.monitor.ServerMonitorEvent;
import net.theskyboardpanel.remote.utils.ServerUtils;

/**
 * 
 * This class is used to monitor all the internal servers
 * 
 * @author Roe
 *
 */
public class ServerMonitor extends Thread
{

	private long lastCheck;
	private long tresshold;
	private long originalTresshold;
	private int failedAttempts;
	private int maxFailedAttempts;
	
	public ServerMonitor()
	{
		
		super("Server Monitor");
		
		Remote.getLogger().log("Starting Server Monitor!");
		
		try
		{
			
			this.setTresshold(Long.parseLong(Remote.getArgumentParser().getArgument("serverMonitorTressHold", "300000").getValue()));
			
		}
		catch(Exception e)
		{
			
			Remote.getLogger().log(Level.WARNING, "serverMonitorTresshold wasn't a number! Falling back to default!");
			this.setTresshold(300000);
			
		}
		
		this.setOriginalTresshold(this.getTresshold());
		this.setFailedAttempts(0);
		
		try
		{
			
			this.setMaxFailedAttempts(Integer.parseInt(Remote.getArgumentParser().getArgument("serverMonitorMaxFails", "3").getValue()));
			
		}
		catch(Exception e)
		{
			
			Remote.getLogger().log(Level.WARNING, "serverMonitorMaxFails wasn't a number! Falling back to default!");
			this.setMaxFailedAttempts(3);
			
		}

		this.setLastCheck(System.currentTimeMillis());
		ServerMonitorEvent event = new ServerMonitorEvent(this.getTresshold(), this.getMaxFailedAttempts());
		Remote.callEvent(event);
		this.setTresshold(event.getTresshold());
		this.setMaxFailedAttempts(event.getMaxFailAttempts());
		
	}
	
	public void run()
	{
		
		while(SKYBoardRemote.getRemote().isRunning() && !(this.isInterrupted()))
		{
			
			if((System.currentTimeMillis() - this.getLastCheck()) >= this.getTresshold())
			{
				
				this.check();
				
			}
			
			try
			{
			
				Thread.sleep(1000);
			
			}
			catch (InterruptedException e)
			{
			
			}
			
		}
		
	}
	
	public void check()
	{
		
		this.setLastCheck(System.currentTimeMillis());
		Remote.getLogger().log("Checking servers");
		int workingServers = 0;
		
		for(Server server : Remote.getServerManager().getServers())
		{
			
			Remote.callEvent(new ServerCheckEvent(server));
			
			if(server.getServerSocket().isClosed())
			{
				
				Remote.getLogger().log("Removing dead server (" + server.getName() + ")");
				Remote.getServerManager().getServers().remove(server);
				Remote.callEvent(new ServerDeadEvent(server));
				
				continue;
				
			}
			
			Remote.getLogger().log("Checking " + server.getName());
			
			if(server.shouldAccept())
			{
				
				Socket socket = null;
				
				try
				{
				
					long responseTime = ServerUtils.pingServer(server);
					
					if(responseTime > 0 && responseTime < 250)
					{
						
						workingServers++;
						Remote.getLogger().log("The " + server.getName() + " responded nice and clear! (" + responseTime + "ms)");
						
					}
					else if(responseTime < 2500)
					{

						workingServers++;
						Remote.getLogger().log(Level.WARNING, "The server: " + server.getName() + " is having a high response time! (" + responseTime + "ms) Is it busy?");
						
					}
					else
					{
						
						Remote.getLogger().log(Level.WARNING, "The server: " + server.getName() + " is having major issues and isn't considered working! (" + responseTime + "ms)");
						
					}
				
				}
				catch (UnknownHostException e)
				{

					Remote.getLogger().log(Level.WARNING, server.getName() + " is unreachable! (" + e.getMessage() + ")");
					
					try
					{
					
						server.close();
					
					}
					catch (IOException e1)
					{
						
						Remote.getLogger().log(e1);
					
					}
				
				}
				catch (IOException e)
				{

					Remote.getLogger().log("Internal error occoured while attemting to ping the " + server.getName());
					Remote.getLogger().log(e);
				
				}
				catch (ClassNotFoundException e)
				{
					
					Remote.getLogger().log("Internal error occoured while attemting to ping the " + server.getName() + ". The server sent a bad response!");
				
				}
				finally
				{
					
					if(socket != null)
					{
						
						try
						{
						
							socket.close();
						
						}
						catch (IOException e)
						{
							
							Remote.getLogger().log("Internal error occoured while attemting to close the pinging socket for the " + server.getName());
							Remote.getLogger().log(e);
						
						}
						
					}
					
				}
				
			}
			
		}
		
		if(workingServers == 0)
		{
			
			this.setFailedAttempts(this.getFailedAttempts() + 1);
			
			Remote.getLogger().log(Level.WARNING, "All servers are unreachable! Waiting " + (this.getMaxFailedAttempts() - this.getFailedAttempts()) + " failed attempts to start a new one!");
			
			if(this.getMaxFailedAttempts() <= this.getFailedAttempts())
			{
				
				Remote.getLogger().log(Level.WARNING, "All servers are unreachable! Starting a new one");
				
				if(Remote.getServerManager().startNewServer(Remote.getArgumentParser().getArgument("hostIp", "0.0.0.0").getValue(), 
						Integer.parseInt(Remote.getArgumentParser().getArgument("hostPort", "24730").getValue()), true) == null)
				{
				
					Remote.getLogger().log(Level.SEVERE, "Unable to startup a new server!");
					
					return;
					
				}
				
				this.setFailedAttempts(0);
				this.setTresshold(this.getOriginalTresshold());
				
			}
			
		}
		else
		{
			
			Remote.getLogger().log("Server Check Complete! There's " + workingServers + " reachable servers!");
			this.setTresshold(this.getOriginalTresshold());
			
		}
		
		this.setLastCheck(System.currentTimeMillis());
		
	}

	public long getLastCheck()
	{
	
		return this.lastCheck;
	
	}

	public void setLastCheck(long lastCheck)
	{
	
		this.lastCheck = lastCheck;
	
	}

	public long getTresshold()
	{
	
		return this.tresshold;
	
	}

	public void setTresshold(long tresshold)
	{
	
		this.tresshold = tresshold;
	
	}

	public long getOriginalTresshold()
	{
	
		return this.originalTresshold;
	
	}

	public void setOriginalTresshold(long originalTresshold)
	{
	
		this.originalTresshold = originalTresshold;
	
	}

	public int getFailedAttempts()
	{
	
		return this.failedAttempts;
	
	}

	public void setFailedAttempts(int failedAttempts) 
	{
	
		this.failedAttempts = failedAttempts;
	
	}

	public int getMaxFailedAttempts()
	{
	
		return this.maxFailedAttempts;
	
	}

	public void setMaxFailedAttempts(int maxFailedAttempts)
	{
	
		this.maxFailedAttempts = maxFailedAttempts;
	
	}
	
}