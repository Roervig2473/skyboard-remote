package net.theskyboardpanel.remote.server;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remote.connection.*;
import net.theskyboardpanel.remote.utils.ServerUtils;
import net.theskyboardpanel.remoteapi.connection.Connection;

/**
 * 
 * This class is used to manage all the servers
 * 
 * @author Roe
 *
 */
public class ServerManager
{

	private CopyOnWriteArrayList<Server> servers;
	private ServerMonitor serverMonitor;
	private ArrayList<Class<? extends Connection>> connectionTypes;

	public ServerManager()
	{
	
		Remote.getLogger().log("Starting Server Manager");
		this.setConnectionTypes(new ArrayList<Class<? extends Connection>>());
		this.setServers(new CopyOnWriteArrayList<Server>());
		
		try
		{
		
			Server server = new Server("0.0.0.0", 24730);
			server.start();
			this.getServers().add(server);
		
		}
		catch (IOException e)
		{

			e.printStackTrace();
		
		}
		
		this.setServerMonitor(new ServerMonitor());
		this.getServerMonitor().start();
		
	}
	
	/**
	 * 
	 * This method is used to get the running server
	 * 
	 * @return the running server
	 */
	public Server getServer()
	{
		
		for(Server server : this.getServers())
		{
			
			if(server.shouldAccept())
			{
				
				return server;
				
			}
			
		}

		return this.startNewServer(Remote.getArgumentParser().getArgument("hostIp", "0.0.0.0").getValue(), 
				Integer.parseInt(Remote.getArgumentParser().getArgument("hostPort", "24730").getValue()), true);
		
	}
	
	public Server startNewServer(String ip, int port, boolean incrementIfInUse)
	{
		
		int maxPortScan = Integer.parseInt(Remote.getArgumentParser().getArgument("startServerMaxPortScan", "30").getValue());
		int count = 0;
		
		while(count < maxPortScan)
		{
			
			for(Server server : this.getServers())
			{
				
				if(server.getIp().equalsIgnoreCase(ip) && server.getPort() == port)
				{
					
					try
					{
					
						server.close();
						count++;
						continue;
					
					}
					catch (IOException e)
					{
						
						Remote.getLogger().log(Level.SEVERE, e);
					
					}
					
				}
				
			}
			
			try
			{
			
				ServerUtils.pingServer(ip, port);

				count++;
				port++;
				
				continue;
			
			}
			catch (UnknownHostException e)
			{
			
			}
			catch (IOException e)
			{

				Remote.getLogger().log(e);
			
			}
			catch (ClassNotFoundException e)
			{
			
				Remote.getLogger().log(e);
			
			}
			
			try
			{
			
				Server server = new Server(ip, port);
				server.start();
				this.getServers().add(server);
				
				return server;
			
			}
			catch (IOException e)
			{
				
				if(e.getMessage().equalsIgnoreCase("Address already in use"))
				{
					
					count++;
					port++;
					
					continue;
					
				}
				
				Remote.getLogger().log(e);
			
			}
			
		}
		
		return null;
		
	}
	
	/**
	 * 
	 * This method is used to close all servers and the server monitor
	 * 
	 */
	public void close()
	{
		
		Remote.getLogger().log("Shutting down the server manager");
		this.getServerMonitor().interrupt();
		
		for(Server server : this.getServers())
		{
			
			try
			{
			
				server.close();
				Remote.getLogger().log("Closed server: " + server.getName());
			
			}
			catch (IOException e)
			{

				Remote.getLogger().log(e);
			
			}
			
			this.getServers().remove(server);
			
		}
		
	}
	
	/**
	 * 
	 * This method is used to get the arraylist of running servers
	 * 
	 * @return the server arraylist
	 */
	public CopyOnWriteArrayList<Server> getServers()
	{
	
		return this.servers;
	
	}

	private void setServers(CopyOnWriteArrayList<Server> servers)
	{
	
		this.servers = servers;
	
	}

	/**
	 * 
	 * This method is used to get the <b>INTERNAL</b> server monitor
	 * 
	 * @return the server monitor
	 */
	public ServerMonitor getServerMonitor()
	{
	
		return this.serverMonitor;
	
	}

	private void setServerMonitor(ServerMonitor serverMonitor)
	{
	
		this.serverMonitor = serverMonitor;
	
	}
	
	/**
	 * 
	 * This method is used to register a new connection type for the servers to use.
	 * 
	 * @param connectionType the connection type you want to register
	 * @return true if it's registered, false if it's not
	 */
	public boolean registerConnectionType(Class<? extends Connection> connectionType)
	{
		
		if(this.getConnectionTypes().contains(connectionType))
		{
			
			return false;
			
		}
		
		for(Class<? extends Connection> clazz : this.getConnectionTypes())
		{
			
			if(clazz.getCanonicalName().equalsIgnoreCase(connectionType.getCanonicalName()))
			{
				
				return false;
				
			}
			
		}
		
		this.getConnectionTypes().add(connectionType);
		
		return true;
		
	}
	
	/**
	 * 
	 * This method is used to get a connection class from the connection identifier
	 * 
	 * @param connectionID the connection identifier recieved by the client
	 * @return the connection class there matches the connection identifier, but will return null if no connection class is available.
	 */
	public Class<? extends Connection> getConnection(String connectionID)
	{
		
		for(Class<? extends Connection> connection : this.getConnectionTypes())
		{
			
			if(connection.getCanonicalName().equalsIgnoreCase(connectionID))
			{
				
				return connection;
				
			}
			
		}
		
		return null;
		
	}

	/**
	 * 
	 * This method is used to get the arraylist of available connection types
	 * 
	 * @return the connectiontype arraylist
	 */
	public ArrayList<Class<? extends Connection>> getConnectionTypes()
	{
	
		return this.connectionTypes;
	
	}

	private void setConnectionTypes(ArrayList<Class<? extends Connection>> connectionTypes)
	{
	
		this.connectionTypes = connectionTypes;
		this.registerConnectionType(ConnectionJava.class);
	
	}

}