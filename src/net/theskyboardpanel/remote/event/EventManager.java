package net.theskyboardpanel.remote.event;

import java.lang.reflect.Method;
import java.util.*;
import java.util.Map.Entry;

import net.theskyboardpanel.remoteapi.event.*;
import net.theskyboardpanel.remoteapi.plugin.Plugin;

public class EventManager
{

	private final HashMap<Class<? extends Event>, ArrayList<EventBinding>> events = new HashMap<Class<? extends Event>, ArrayList<EventBinding>>();
	
	@SuppressWarnings("unchecked")
	public void registerListener(Listener listener, Plugin plugin)
	{

		for(Method method : listener.getClass().getMethods())
		{
			
			if(!method.isAnnotationPresent(EventHandler.class))
			{

				continue;
				
			}
			
			EventHandler handler = method.getAnnotation(EventHandler.class);
			
			if(method.getParameterTypes().length != 1)
			{
				
				plugin.getLogger().log("[EventManager] Attempted to register invalid event listener method: " + listener.getClass().getCanonicalName() + ", " + method.getName() + " to many or not enough parameters");
				
				continue;
				
			}
			
			Class<? extends Event> event = null;
			
			try
			{

				event = (Class<? extends Event>) method.getParameterTypes()[0];
				
			}
			catch(Exception e)
			{
				
				plugin.getLogger().log("[EventManager] Attempted to register invalid event listener method: " + listener.getClass().getCanonicalName() + ", " + method.getName() + " parameter isn't an event!");
				
				continue;
				
			}
			
			ArrayList<EventBinding> bindings = this.getEventBinding(event);
			
			if(bindings != null)
			{
				
				for(EventBinding binding : bindings)
				{

					if((binding.getMethod().getName().equals(method.getName())) && (binding.getListener().getClass().getCanonicalName().equals(listener.getClass().getCanonicalName())))
					{
						
						plugin.getLogger().log("[EventManager] Attempted to register a already registered listener: " + listener.getClass().getCanonicalName() + ", " + method.getName());
						
						continue;
						
					}
						
				}
				
			}
			
			EventBinding binding = new EventBinding(listener, method, plugin, handler);
			boolean added = false;
			
			forLoop:
			
			for(Entry<Class<? extends Event>, ArrayList<EventBinding>> entry : this.getEvents().entrySet())
			{

				Class<? extends Event> loopEvent = entry.getKey();
				
				if(loopEvent.getCanonicalName().equals(event.getCanonicalName()))
				{
					
					entry.getValue().add(binding);
					added = true;
					
					break forLoop;
					
				}
				
			}
			
			if(!added)
			{
				
				bindings = new ArrayList<EventBinding>();
				bindings.add(binding);
				this.getEvents().put(event, bindings);
				
			}
			
		}
		
	}
	
	public void call(Event event)
	{

		ArrayList<EventBinding> bindings = this.getEventBinding(event.getClass());
		
		if(bindings == null)
		{
			
			return;
			
		}
		
		this.callLowest(event, bindings);
		this.callLow(event, bindings);
		this.callRegular(event, bindings);
		this.callHigh(event, bindings);
		this.callHighest(event, bindings);
		this.giveResult(event, bindings);
		
	}
	
	private void callLowest(Event event, ArrayList<EventBinding> bindings)
	{

		this.call(event, bindings, EventPriority.LOWEST);
		
	}
	
	private void callLow(Event event, ArrayList<EventBinding> bindings)
	{
		
		this.call(event, bindings, EventPriority.LOW);
		
	}
	
	private void callRegular(Event event, ArrayList<EventBinding> bindings)
	{
		
		this.call(event, bindings, EventPriority.REGULAR);
		
	}
	
	private void callHigh(Event event, ArrayList<EventBinding> bindings)
	{
		
		this.call(event, bindings, EventPriority.HIGH);
		
	}
	
	private void callHighest(Event event, ArrayList<EventBinding> bindings)
	{
		
		this.call(event, bindings, EventPriority.HIGHEST);
		
	}
	
	private void giveResult(Event event, ArrayList<EventBinding> bindings)
	{
		
		this.call(event, bindings, EventPriority.RESULT);
		
	}
	
	private void call(Event event, ArrayList<EventBinding> bindings, EventPriority priority)
	{
		
		for(EventBinding binding : bindings)
		{
			
			if(binding.getHandler().priority() != priority)
			{

				continue;
				
			}
			
			try
			{
				
				if(event instanceof Cancelable)
				{
					
					if((((Cancelable) event).isCancelled()) && (binding.getHandler().ignoreCancelled() == false))
					{
						
						continue;
						
					}
					
				}
				
				if(priority == EventPriority.RESULT)
				{

					Event result = event;
					binding.call(result);
					event.getHandlers().add(binding.getListener());
					
					continue;
					
				}
				
				binding.call(event);
				event.getHandlers().add(binding.getListener());
				
			}
			catch(Exception e)
			{
				
				e.printStackTrace();
				
			}
			
		}
		
	}

	public ArrayList<EventBinding> getEventBinding(Class<? extends Event> event)
	{
		
		ArrayList<EventBinding> toReturn = new ArrayList<EventBinding>();
		
		for(Entry<Class<? extends Event>, ArrayList<EventBinding>> entry : this.getEvents().entrySet())
		{
			
			Class<? extends Event> entryEvent = entry.getKey();
			
			if(event.getCanonicalName().equals(entryEvent.getCanonicalName()))
			{
				
				return entry.getValue();
				
			}
			
			try
			{
				
				@SuppressWarnings("unchecked")
				ArrayList<EventBinding> bindings = Event.class.isAssignableFrom(event.getSuperclass()) ? this.getEventBinding((Class<? extends Event>) event.getSuperclass()) : null;

				if(bindings != null)
				{
					
					toReturn.addAll(bindings);
					
				}
				
			}
			catch(Exception e)
			{
				
			}
			
		}
		
		return toReturn;
		
	}
	
	public HashMap<Class<? extends Event>, ArrayList<EventBinding>> getEvents()
	{
	
		return this.events;
	
	}
	
}