package net.theskyboardpanel.remote.event.command;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remoteapi.command.Command;
import net.theskyboardpanel.remoteapi.event.*;

/**
 * 
 * This event is triggered when the console is firing a command
 * 
 * @author Roe
 *
 */
public class ConsoleCommandEvent extends CommandEvent implements Cancelable
{
	
	/**
	 * 
	 * @param command the command
	 * @param args the args
	 */
	public ConsoleCommandEvent(Command command, String[] args)
	{
		
		super(Remote.getConsole(), command, args);
		
	}

}