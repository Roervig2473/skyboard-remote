/**
 * 
 */
package net.theskyboardpanel.remote.event.command;

import net.theskyboardpanel.remoteapi.command.Command;
import net.theskyboardpanel.remoteapi.command.CommandSender;
import net.theskyboardpanel.remoteapi.event.Cancelable;

/**
 * 
 * This event is triggered when a user is firing a command
 * 
 * @author Roe
 *
 */
public class UserCommandEvent extends CommandEvent implements Cancelable
{

	/**
	 * 
	 * @param sender the command sender
	 * @param command the command
	 * @param args the command arguments
	 */
	public UserCommandEvent(CommandSender sender, Command command, String[] args)
	{
		
		super(sender, command, args);
		
	}
	
}