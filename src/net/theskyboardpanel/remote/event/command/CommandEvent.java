package net.theskyboardpanel.remote.event.command;

import net.theskyboardpanel.remoteapi.command.Command;
import net.theskyboardpanel.remoteapi.command.CommandSender;
import net.theskyboardpanel.remoteapi.event.*;

/**
 * 
 * This event is triggered when a command is fired
 * 
 * @author Roe
 *
 */
public class CommandEvent extends Event implements Cancelable
{

	private boolean cancelled = false;
	private CommandSender sender;
	private Command command;
	private String[] args;
	
	/**
	 * 
	 * @param sender the command sender
	 * @param command the command
	 * @param args the command arguments
	 */
	public CommandEvent(CommandSender sender, Command command, String[] args)
	{
		
		this.setSender(sender);
		this.setCommand(command);
		this.setArgs(args);
		
	}
	
	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.event.Cancelable#isCancelled()
	 */
	@Override
	public boolean isCancelled()
	{

		return this.cancelled;
	
	}

	/**
	 * @see net.theskyboardpanel.remoteapi.event.Cancelable#setCancelled(boolean)
	 */
	@Override
	public void setCancelled(boolean cancelled)
	{

		this.cancelled = cancelled;
		
	}

	/**
	 * 
	 * This method is used to get the sender of the command
	 * 
	 * @return the command sender
	 */
	public CommandSender getSender()
	{
	
		return this.sender;
	
	}

	/**
	 * 
	 * This method is used to set the sender of the command
	 * 
	 * @param sender the command sender
	 */
	public void setSender(CommandSender sender)
	{
	
		this.sender = sender;
	
	}

	/**
	 * 
	 * This method is used to get the command
	 * 
	 * @return the command
	 */
	public Command getCommand()
	{
	
		return this.command;
	
	}

	/**
	 * 
	 * This method is used to set the command
	 * 
	 * @param command the command
	 */
	public void setCommand(Command command)
	{
	
		this.command = command;
	
	}

	/**
	 * 
	 * This method is used to get command arguments
	 * 
	 * @return the command arguments
	 */
	public String[] getArgs()
	{
			
		return this.args;
	
	}

	/**
	 * 
	 * This method is used to set the command arguments
	 * 
	 * @param args the command arguments
	 */
	public void setArgs(String[] args)
	{
	
		this.args = args;
	
	}

}