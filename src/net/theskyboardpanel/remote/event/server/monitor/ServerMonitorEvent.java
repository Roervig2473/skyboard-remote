package net.theskyboardpanel.remote.event.server.monitor;

import net.theskyboardpanel.remoteapi.event.Event;

/**
 * 
 * This event is fired when the server monitor is defined
 * 
 * @author Roe
 *
 */
public class ServerMonitorEvent extends Event
{

	private long tresshold;
	private int maxFailAttempts;
	
	public ServerMonitorEvent(long tresshold, int maxFailAttempts)
	{
		
		this.setTresshold(tresshold);
		this.setMaxFailAttempts(maxFailAttempts);
		
	}
	
	public long getTresshold() 
	{

		return this.tresshold;
	
	}
	
	public void setTresshold(long tresshold)
	{
	
		this.tresshold = tresshold;
	
	}
	
	public int getMaxFailAttempts()
	{
	
		return this.maxFailAttempts;
	
	}
	
	public void setMaxFailAttempts(int maxFailAttempts)
	{
	
		this.maxFailAttempts = maxFailAttempts;
	
	}
	
}