package net.theskyboardpanel.remote.event.server.monitor;

import net.theskyboardpanel.remote.server.Server;
import net.theskyboardpanel.remoteapi.event.Event;

/**
 * 
 * This event is triggered when a server is getting checked by the server monitor
 * 
 * @author Roe
 *
 */
public class ServerCheckEvent extends Event
{

	private Server server;
	
	public ServerCheckEvent(Server server)
	{
		
		this.setServer(server);
		
	}

	public Server getServer()
	{
	
		return this.server;
	
	}

	public void setServer(Server server)
	{
	
		this.server = server;
	
	}
	
}