package net.theskyboardpanel.remote.event.server.monitor;

import net.theskyboardpanel.remote.server.Server;
import net.theskyboardpanel.remoteapi.event.Event;

/**
 * 
 * This event is triggered when the server monitor marks a server dead
 * 
 * @author Roe
 *
 */
public class ServerDeadEvent extends Event
{

	private Server server;
	
	public ServerDeadEvent(Server server)
	{
		
		this.setServer(server);
		
	}

	public Server getServer()
	{
	
		return this.server;
	
	}

	public void setServer(Server server)
	{
	
		this.server = server;
	
	}
	
}