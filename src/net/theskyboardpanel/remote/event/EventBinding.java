package net.theskyboardpanel.remote.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.theskyboardpanel.remoteapi.event.*;
import net.theskyboardpanel.remoteapi.plugin.Plugin;

public class EventBinding
{

	private final Listener listener;
	private final Method method;
	private final Plugin plugin;
	private final EventHandler handler;
	
	public EventBinding(Listener listener, Method method, Plugin plugin, EventHandler handler)
	{
		
		this.listener = listener;
		this.method = method;
		this.plugin = plugin;
		this.handler = handler;
		
	}

	public void call(Object... args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
	{
		
		this.getMethod().invoke(this.getListener(), args);

		
	}
	
	public Listener getListener()
	{
	
		return this.listener;
	
	}

	public Method getMethod()
	{
	
		return this.method;
	
	}

	public Plugin getPlugin()
	{
	
		return this.plugin;
	
	}

	public EventHandler getHandler()
	{
	
		return this.handler;

	}	
	
}