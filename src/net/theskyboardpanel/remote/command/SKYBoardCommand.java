package net.theskyboardpanel.remote.command;

import net.theskyboardpanel.remoteapi.command.Command;
import net.theskyboardpanel.remoteapi.command.CommandExecutor;
import net.theskyboardpanel.remoteapi.command.CommandSender;

public class SKYBoardCommand implements Command
{

	private String name;
	private String command;
	private String description;
	private String[] alternatives;
	private CommandExecutor executor;
	
	public SKYBoardCommand(String name, String command, String description, String[] alternatives)
	{
		
		this.name = name;
		this.command = command;
		this.description = description;
		this.alternatives = alternatives;
		
	}
	
	@Override
	public String getName()
	{
		
		return this.name;
	
	}

	@Override
	public void setName(String name)
	{

	}

	@Override
	public String getCommand()
	{

		return this.command;

	}

	@Override
	public void setCommand(String command)
	{

	}

	@Override
	public String getDescription()
	{
		
		return this.description;
	
	}

	@Override
	public void setDescription(String description)
	{

	}

	@Override
	public String[] getAlternatives()
	{
		
		return this.alternatives;
	
	}

	@Override
	public void setAlternatives(String[] alternatives)
	{

	}

	@Override
	public boolean executeCommand(CommandSender sender, String[] args)
	{

		return this.getCommandExecutor().onCommand(sender, this, args);
	
	}

	@Override
	public CommandExecutor getCommandExecutor()
	{

		return this.executor;
	
	}

	@Override
	public void setCommandExecutor(CommandExecutor executor)
	{
		
	}

}
