package net.theskyboardpanel.remote.command;

import java.util.ArrayList;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remote.event.command.*;
import net.theskyboardpanel.remote.user.Console;
import net.theskyboardpanel.remote.utils.ReflectionUtils;
import net.theskyboardpanel.remoteapi.command.*;
import net.theskyboardpanel.remoteapi.permission.*;
import net.theskyboardpanel.remoteapi.plugin.Plugin;

public class CommandManager
{

	private ArrayList<Command> commands;
	
	public CommandManager()
	{
		
		this.setCommands(new ArrayList<Command>());
		
	}

	public Command registerCommand(Command command, Plugin plugin)
	{
		
		try
		{
			
			if(this.getCommand(command.getCommand(), true) != null)
			{
				
				return null;
				
			}
			
		}
		catch(UnknownCommandException e)
		{
			
		}
		
		this.setCommandExectutor(command, plugin);
		this.getCommands().add(command);
		
		return command;
		
	}
	
	public void setCommandExectutor(Command command, CommandExecutor executor)
	{
		
		command.setCommandExecutor(executor);
		
		if(command instanceof SKYBoardCommand)
		{
			
			ReflectionUtils.setField(SKYBoardCommand.class, "executor", executor, command);
			
		}
		
	}
	
	public Command getCommand(String commandString, boolean searchName) throws UnknownCommandException
	{
		
		for(Command command : this.getCommands())
		{
			
			if(command.getCommand().equalsIgnoreCase(commandString))
			{
				
				return command;
				
			}
			
			for(String alias : command.getAlternatives())
			{
				
				if(alias.equalsIgnoreCase(commandString))
				{
					
					return command;
					
				}
				
			}
			
			if(searchName)
			{
				
				if(command.getName().equalsIgnoreCase(commandString))
				{
					
					return command;
					
				}
				
			}
			
		}
		
		throw new UnknownCommandException(commandString);
		
	}
	
	public boolean onCommand(CommandSender sender, String commandString, String[] args) throws InsufficientPermissionsException
	{
		
		try
		{
			
			CommandEvent event = null;
			
			if(sender instanceof Console)
			{
				
				event = new ConsoleCommandEvent(this.getCommand(commandString, true), args);
				
			}
			else
			{

				event = new UserCommandEvent(sender, this.getCommand(commandString, true), args);
				
			}
			
			Remote.callEvent(event);
			
			if(event.isCancelled())
			{
				
				return true;
				
			}
			
			return event.getCommand().executeCommand(sender, args);
			
		}
		catch(UnknownCommandException e)
		{
			
			sender.sendMessage("Unknown Command: " + commandString);
			
			return false;
			
		}
		catch(InsufficientPermissionsException e)
		{
			
			throw e;
			
		}
		catch(Exception e)
		{
			
			Remote.getLogger().log(e);
			sender.sendMessage("An internal error occoured while performing this command");
			
		}
		
		return false;
	
	}

	public ArrayList<Command> getCommands()
	{
		
		return this.commands;
	
	}

	public void setCommands(ArrayList<Command> commands)
	{
	
		this.commands = commands;
	
	}
	
}