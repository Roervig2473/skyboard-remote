package net.theskyboardpanel.remote.command.commands;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remote.command.SKYBoardCommand;
import net.theskyboardpanel.remote.utils.ArrayUtils;
import net.theskyboardpanel.remoteapi.command.Command;
import net.theskyboardpanel.remoteapi.command.CommandExecutor;
import net.theskyboardpanel.remoteapi.command.CommandSender;
import net.theskyboardpanel.remoteapi.permission.InsufficientPermissionsException;

/**
 * @author Roe
 *
 */
public class CommandShutdown extends SKYBoardCommand implements CommandExecutor
{
	
	public CommandShutdown()
	{
	
		super("Shutdown", "shutdown", "This command is used to shut down the server", new String[]{"stop"});

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.command.CommandExecutor#onCommand(net.theskyboardpanel.remoteapi.command.CommandSender, net.theskyboardpanel.remoteapi.command.Command, java.lang.String[])
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String[] args) throws InsufficientPermissionsException
	{
		
		Remote.getLogger().log(sender.getUsername() + " have requested a shutdown");
		
		if(!sender.hasPermission("skyboardremote.shutdown"))
		{
			
			throw new InsufficientPermissionsException(sender);
			
		}
	
		if(args.length > 0)
		{
			
			Remote.getLogger().log("Shutting down: " + ArrayUtils.arrayToString(args, " "));
			
		}
		
		Remote.shutdown();
		
		return true;
	
	}

}