/**
 * 
 */
package net.theskyboardpanel.remote.command.commands;

import java.io.File;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remote.SKYBoardRemote;
import net.theskyboardpanel.remote.command.SKYBoardCommand;
import net.theskyboardpanel.remote.server.ServerManager;
import net.theskyboardpanel.remoteapi.command.*;
import net.theskyboardpanel.remoteapi.permission.InsufficientPermissionsException;

/**
 * @author Roe
 *
 */
public class CommandReload extends SKYBoardCommand implements CommandExecutor
{

	public CommandReload()
	{
	
		super("Reload", "reload", "This command is used to reload the server", new String[0]);

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.command.CommandExecutor#onCommand(net.theskyboardpanel.remoteapi.command.CommandSender, net.theskyboardpanel.remoteapi.command.Command, java.lang.String[])
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String[] args) throws InsufficientPermissionsException
	{
		
		if(!sender.hasPermission("remote.command.reload"))
		{
			
			throw new InsufficientPermissionsException(sender);
			
		}
		
		Remote.getLogger().log("Reloading");
		Remote.getServerManager().close();
		Remote.getPluginManager().clearPlugins();
		Remote.getPluginManager().loadPlugins(new File(SKYBoardRemote.getRemote().getArgumentParser().getArgument("pluginsDir", "plugins").getValue()));;
		SKYBoardRemote.getRemote().setServerManager(new ServerManager());
		Remote.getLogger().log("Reload Complete");
		
		return true;
	
	}

}