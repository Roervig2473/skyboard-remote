/**
 * 
 */
package net.theskyboardpanel.remote.command.commands;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remote.command.SKYBoardCommand;
import net.theskyboardpanel.remoteapi.command.*;
import net.theskyboardpanel.remoteapi.permission.InsufficientPermissionsException;

/**
 * @author Roe
 *
 */
public class CommandCheck extends SKYBoardCommand implements CommandExecutor
{

	public CommandCheck()
	{
	
		super("Server Monitor Check", "check", "This command is used to make the server monitor reset the timer and check instantly", new String[0]);

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.command.CommandExecutor#onCommand(net.theskyboardpanel.remoteapi.command.CommandSender, net.theskyboardpanel.remoteapi.command.Command, java.lang.String[])
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String[] args) throws InsufficientPermissionsException
	{
		
		if(!sender.hasPermission("remote.command.check"))
		{
			
			throw new InsufficientPermissionsException(sender);
			
		}
		
		Remote.getServerManager().getServerMonitor().check();

		return true;
	
	}

}