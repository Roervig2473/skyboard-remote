package net.theskyboardpanel.remote.command;

import net.theskyboardpanel.remoteapi.command.CommandException;

public class UnknownCommandException extends CommandException
{

	private static final long serialVersionUID = 8545602660089002193L;

	public UnknownCommandException(String command)
	{
		
		super("Unknown command " + command);
	
	}

}