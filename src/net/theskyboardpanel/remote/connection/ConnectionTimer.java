package net.theskyboardpanel.remote.connection;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remoteapi.connection.Connection;
import net.theskyboardpanel.remoteapi.connection.exceptions.ConnectionException;

public class ConnectionTimer extends Thread
{

	private final Connection connection;
	private long lastRecievedPacket;
	private long tresshold;
	
	public ConnectionTimer(Connection connection)
	{
		
		this(connection, 150000);
		
	}
	
	public ConnectionTimer(Connection connection, long tresshold)
	{
		
		this.connection = connection;
		this.setLastRecievedPacket(System.currentTimeMillis());
		
		try
		{
		
			this.setTresshold(Long.parseLong(Remote.getArgumentParser().getArgument("connectionTimerTresshold", "" + tresshold).getKey()));
		
		}
		catch(NumberFormatException e)
		{
			
			this.setTresshold(tresshold);
			
		}
			
	}
	
	public void run()
	{
		
		while(this.getConnection().getSocket().isClosed())
		{
			
			if((System.currentTimeMillis() - this.getLastRecievedPacket()) >= this.getTresshold())
			{
			
				Remote.getLogger().log("Closing " + this.getConnection().getName() + " (TIMEOUT)");
				
				try
				{
				
					this.getConnection().close();
					
					return;
				
				}
				catch (ConnectionException e)
				{

					Remote.getLogger().log(e);
				
				}
				
			}
			
		}
		
	}

	public Connection getConnection()
	{
	
		return this.connection;
	
	}

	public long getLastRecievedPacket()
	{
	
		return this.lastRecievedPacket;
	
	}

	public void setLastRecievedPacket(long lastRecievedPacket)
	{
	
		this.lastRecievedPacket = lastRecievedPacket;
	
	}

	public long getTresshold()
	{
	
		return this.tresshold;
	
	}

	public void setTresshold(long tresshold)
	{
	
		this.tresshold = tresshold;
	
	}
	
}