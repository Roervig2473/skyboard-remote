package net.theskyboardpanel.remote.connection;

import java.io.*;
import java.net.*;
import java.util.logging.Level;

import net.theskyboardpanel.remote.Remote;
import net.theskyboardpanel.remote.user.User;
import net.theskyboardpanel.remoteapi.connection.exceptions.ConnectionException;
import net.theskyboardpanel.remoteapi.packet.Packet;
import net.theskyboardpanel.remoteapi.packet.packets.authentication.PacketAuthenticate;
import net.theskyboardpanel.remoteapi.packet.packets.common.PacketCloseConnection;
import net.theskyboardpanel.remoteapi.packet.packets.status.*;

/**
 * @author Roe
 *
 */
public class ConnectionJava extends User
{

	private Packet lastPacket;

	public ConnectionJava(Socket socket, ObjectOutputStream out, ObjectInputStream in)
	{
	
		super(socket, out, in);

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.connection.Connection#getPacket()
	 */
	@Override
	public Packet getPacket() throws ConnectionException
	{

		return this.lastPacket;
	
	}

	public void run()
	{
		
		Remote.getLogger().log("A java connection have been made!");
		
		while(!this.getSocket().isClosed())
		{
			
			try
			{
			
				Packet packet = this.recievePacket();
				
				if(packet instanceof PacketPing)
				{
					
					this.sendPacket(new PacketPong());
					this.close();
					
				}
				else if(packet instanceof PacketAuthenticate)
				{
					
					this.authenticate();
					
				}
				else if(packet instanceof PacketCloseConnection)
				{
				
					this.sendPacket(new PacketCloseConnection());
					this.close();
					
				}
			
			}
			catch (ConnectionException e)
			{
				
				Remote.getLogger().log(Level.SEVERE, e);
			
			}
			
		}
		
	}
	
	public void authenticate()
	{
		
		if(!(this.getLastPacket() instanceof PacketAuthenticate))
		{
			
			return;
			
		}
		
	}
	
	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.connection.Connection#recievePacket()
	 */
	@Override
	public Packet recievePacket() throws ConnectionException
	{
		
		try
		{
		
			this.setLastPacket((Packet) this.getIn().readObject());
			
			return this.getLastPacket();
	
		}
		catch (IOException e)
		{

			Remote.getLogger().log(Level.SEVERE, e);
			
			throw new ConnectionException(this);
			
		}
		catch (ClassNotFoundException e)
		{

			Remote.getLogger().log(Level.SEVERE, e);
			
			throw new ConnectionException(this);
			
		}
	
	}

	/**
	 *
	 * @see net.theskyboardpanel.remoteapi.connection.Connection#sendPacket(net.theskyboardpanel.remoteapi.packet.Packet)
	 */
	@Override
	public void sendPacket(Packet packet) throws ConnectionException
	{
		
		try
		{
		
			this.getOut().writeObject(packet);
			this.getOut().flush();
	
		}
		catch (IOException e)
		{
			
			Remote.getLogger().log(Level.SEVERE, e.getMessage());
			
			throw new ConnectionException(this);
			
		}

	}

	/**
	 * 
	 * @see net.theskyboardpanel.remoteapi.connection.Connection#close()
	 */
	@Override
	public void close() throws ConnectionException
	{

		try 
		{
		
			this.getSocket().close();
		
		}
		catch (IOException e)
		{
			
			Remote.getLogger().log(e);
		
		}
		
	}

	public Packet getLastPacket()
	{
	
		return this.lastPacket;
	
	}

	public void setLastPacket(Packet lastPacket)
	{
	
		this.lastPacket = lastPacket;
	
	}

}