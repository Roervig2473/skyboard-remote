package com.javaportals.argumentsparser;

public class BadArgumentsException extends RuntimeException
{

	private static final long serialVersionUID = 5072888249621951649L;

	public BadArgumentsException(String[] args)
	{
		
		super("Bad arguments supplied for the parser! (" + Utilities.arrayToString(args, " -|- "));
		
	}
	
}