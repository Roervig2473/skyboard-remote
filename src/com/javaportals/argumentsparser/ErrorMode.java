package com.javaportals.argumentsparser;

public enum ErrorMode
{

	SILENT,
	THROW;
	
}