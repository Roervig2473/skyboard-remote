package com.javaportals.argumentsparser;

import java.util.ArrayList;

public class MissingRequiredArgumentsException extends RuntimeException
{

	private static final long serialVersionUID = -5036070474255267388L;

	public MissingRequiredArgumentsException(ArrayList<String> missingArguments)
	{
		
		super("Some required arguments wasn't set! (" + Utilities.arrayToString(missingArguments, ", ") + ")");
		
	}
	
}