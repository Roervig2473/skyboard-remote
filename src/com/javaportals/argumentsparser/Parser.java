package com.javaportals.argumentsparser;

import java.util.ArrayList;

public class Parser
{

	private String[] args;
	private String argumentSeperator;
	private String argumentStart;
	private boolean override;
	private boolean parsed;
	private ErrorMode errorMode;
	private String lastError;
	private int errorCode;
	private ArrayList<Argument> arguments;
	private ArrayList<String> requiredArguments;
	
	public Parser(String[] args)
	{
		
		this.setArgs(args);
		this.setDefaults();
		
	}
	
	public boolean checkArguments()
	{
		
		return true;
		
	}
	
	public boolean parse()
	{
		
		if(this.getArgs().length == 0)
		{
			
			this.setParsed(true);
			
			return true;
			
		}
	
		if(!this.checkArguments())
		{

			this.throwError(new BadArgumentsException(this.getArgs()));
			return false;
			
		}
		
		ArrayList<String> required = this.getRequiredArguments();
		String argsString = Utilities.arrayToString(this.getArgs(), " ");
		String args[] = argsString.split(this.getArgumentStart());
		
		for(int dash = 1; dash < args.length; dash++)
		{
			
			String arg = args[dash];
			String[] finalArgs = arg.split(this.getArgumentSeperator());
			
			if(arg.endsWith(" "))
			{
				
				finalArgs[1] = finalArgs[1].substring(0, finalArgs[1].length() - 1);
				
			}
			
			required.remove(finalArgs[0]);
			
			this.addArgument(new Argument(finalArgs[0], finalArgs[1]));
			
		}
		
		if(required.size() != 0)
		{
			
			this.setParsed(false);
			
			this.throwError(new MissingRequiredArgumentsException(required));
			
			return false;
			
		}
		
		this.setParsed(true);
		
		return true;
		
	}
	
	public void throwError(RuntimeException e)
	{
		
		switch(this.getErrorMode())
		{
			
			case SILENT:
				
				this.setLastError(e.getMessage());
				this.setErrorCode(1);
				
				break;
				
			case THROW:
				
				throw e;
				
			default:
				
				throw e;
			
		}
		
	}
		
	public void setDefaults()
	{
		
		this.setDefaultArgumentSeperator();
		this.setDefaultArgumentStart();
		this.setDefaultOverride();
		this.setParsed(false);
		this.setDefaultErrorMode();
		this.setDefaultLastError();
		this.setDefaultErrorCode();
		this.setArguments(new ArrayList<Argument>());
		this.setRequiredArguments(new ArrayList<String>());
		
	}
	
	public String[] getArgs()
	{
		
		return this.args;
		
	}
	
	public void setArgs(String[] args)
	{
		
		this.args = args;
		
	}
	
	public String getArgumentSeperator()
	{
	
		return this.argumentSeperator;
		
	}
	
	public void setArgumentSeperator(String argumentSeperator)
	{
	
		this.argumentSeperator = argumentSeperator;
	
	}
	
	public void setDefaultArgumentSeperator()
	{
		
		this.setArgumentSeperator(":");
		
	}
	
	public String getArgumentStart()
	{
	
		return this.argumentStart;
	
	}
	
	public void setArgumentStart(String argumentStart)
	{
	
		this.argumentStart = argumentStart;
	
	}

	public void setDefaultArgumentStart()
	{
	
		this.setArgumentStart("--");
	
	}
	
	public boolean isOverriding()
	{
		
		return this.override;
		
	}
	
	public void setOverride(boolean override)
	{
	
		this.override = override;
		
	}
	
	public void setDefaultOverride()
	{
		
		this.setOverride(false);
		
	}
	
	public boolean isParsed()
	{
		
		return this.parsed;
		
	}
	
	private void setParsed(boolean parsed)
	{
		
		this.parsed = parsed;
		
	}
	
	public ErrorMode getErrorMode()
	{
		
		return this.errorMode;
		
	}
	
	public void setErrorMode(ErrorMode errorMode)
	{
		
		this.errorMode = errorMode;
		
	}
	
	public void setDefaultErrorMode()
	{
		
		this.setErrorMode(ErrorMode.THROW);
		
	}
	
	public String getLastError()
	{
		
		return this.lastError;
		
	}
	
	private void setLastError(String lastError)
	{
		
		this.lastError = lastError;
		
	}
	
	private void setDefaultLastError()
	{
		
		this.setLastError("");
		
	}
	
	public int getErrorCode()
	{
		
		return this.errorCode;
		
	}
	
	private void setErrorCode(int errorCode)
	{
		
		this.errorCode = errorCode;
		
	}
	
	private void setDefaultErrorCode()
	{
		
		this.setErrorCode(0);
		
	}
	
	public ArrayList<Argument> getArguments()
	{
		
		if(!this.isParsed())
		{
			
			this.throwError(new ArgumentsNotParsed());
			return null;
			
		}
		
		return this.arguments;
		
	}
	
	private void addArgument(Argument argument)
	{
		
		this.arguments.add(argument);
		
	}
	
	private void setArguments(ArrayList<Argument> arguments)
	{
		
		this.arguments = arguments;
		
	}
	
	public ArrayList<String> getRequiredArguments()
	{
		
		return this.requiredArguments;
		
	}
	
	public void setRequiredArguments(ArrayList<String> requiredArguments)
	{
		
		this.requiredArguments = requiredArguments;
		
	}
	
	public void addRequiredParameter(String parameter)
	{
		
		this.getRequiredArguments().add(parameter);
		
	}
	
	public void addRequiredParameters(String[] args)
	{
		
		for(String arg : args)
		{
			
			this.getRequiredArguments().add(arg);
			
		}
		
	}
	
	public Argument getArgument(String key)
	{
		
		for(Argument argument : this.getArguments())
		{
			
			if(argument.getKey().equalsIgnoreCase(key))
			{
				
				return argument;
				
			}
			
		}
		
		this.throwError(new NoSuchArgumentException(key));
		return null;
		
	}
	
	public Argument getArgument(String key, String value)
	{
		
		try
		{
			
			Argument argument = this.getArgument(key);
			
			if(argument != null)
			{
				
				return argument;
				
			}
			
		}
		catch(NoSuchArgumentException e)
		{
			
		}
		
		return new Argument(key, value);
		
	}
	
}