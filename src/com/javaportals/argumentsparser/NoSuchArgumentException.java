package com.javaportals.argumentsparser;

public class NoSuchArgumentException extends RuntimeException
{

	private static final long serialVersionUID = 7617651768450926124L;

	public NoSuchArgumentException(String key)
	{
		
		super("Theres no such argument with the key: " + key);
		
	}
	
}