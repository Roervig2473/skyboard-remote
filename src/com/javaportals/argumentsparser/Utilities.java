package com.javaportals.argumentsparser;

import java.util.ArrayList;

public class Utilities
{

	public static String arrayToString(String[] array, String splitter)
	{
		
		String result = "";
		
		for(int roe = 0; roe < array.length; roe++)
		{
			
			result += array[roe] + splitter;
			
		}
		
		return result.substring(0, result.length() - splitter.length());
		
	}

	public static String arrayToString(ArrayList<String> array, String splitter)
	{
		
		return Utilities.arrayToString((String[]) array.toArray(), splitter);
		
	}
	
}