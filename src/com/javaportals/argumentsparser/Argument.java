package com.javaportals.argumentsparser;

public class Argument
{

	private String key;
	private String value;
	
	public Argument(String key, String value)
	{
		
		this.setKey(key);
		this.setValue(value);
		
	}

	
	public String getKey()
	{
	
		return this.key;
	
	}

	public void setKey(String key)
	{
	
		this.key = key;
	
	}

	public String getValue()
	{
	
		return this.value;
	
	}
	
	public void setValue(String value)
	{
	
		this.value = value;
	
	}
	
}