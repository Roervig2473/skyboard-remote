package com.javaportals.argumentsparser;

public class ArgumentsNotParsed extends RuntimeException
{

	private static final long serialVersionUID = 7987674904851203160L;

	public ArgumentsNotParsed()
	{
		
		super("You haven't parsed your arguments yet! please run the parse() function");
		
	}
	
}